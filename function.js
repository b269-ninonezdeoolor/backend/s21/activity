let users = ["Dwayne Johnson", "Steve Austin", "Kurt Angle", "Dave Bautista"];

console.log("Original Array: ")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -Function should be able to receive a single argument.
        -Add the input data at the end of the array.
        -The function should not be able to return data.
        -Invoke and add an argument to be passed in the function.
        -Log the users array in the console.
*/

	function addUserToArray(user)
	{
		users.push(user);
	}

	addUserToArray("John Cena");
	console.log(users);

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -Function should be able to receive a single argument.
        -Return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -Log the itemFound variable in the console.
*/

	let item_found;

	function getItembyIndex(index)
	{
		return users[index];
	}

	item_found = getItembyIndex(2);
	console.log(item_found);

/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.
*/

	function deleteLastUser(users)
	{
	  let lastUser = users[users.length - 1];
	  users.length--;
	  return lastUser;
	}

	console.log(deleteLastUser(users));

/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

	function updateArrayItem(update, index)
	{
	  users[index] = update;
	}

	console.log(users);
	updateArrayItem("Triple H", 3);
	console.log(users);

/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

	function deleteAllItems(array) {
	  array.length = 0;
	}

	deleteAllItems(users);
	console.log(users);

/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
        -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.
*/

	function checkIfUsersEmpty(array)
	{
	  if (array.length > 0)
	  {
	    return false;
	  }
	  else
	  {
	    return true;
	  }
	}

	let isUsersEmpty = checkIfUsersEmpty(users);
	console.log(isUsersEmpty);